# Star Wars 0.1.0

## Installation

Clone this repo into a new project folder and run install script.
(You will probably want to delete the .git/ directory and start version control afresh.)

With npm

```sh
$ git clone git@bitbucket.org:gabrielguita/starwars.git new-project
$ cd new-project
$ npm install
```

## Developing / Running

Start the project with

```sh
$ npm start
```

Open http://localhost:3000 in browser if doesn't start automatically

## Static assets

Just add to `src/assets/`

<hr />

## ES6

This starter includes [Babel](https://babeljs.io/) so you can directly use ES6 code.

## Changelog

-   0.1.0 - initial upload for Flickr gallery

## How it works

`npm run dev` maps to `webpack-dev-server --hot --colors --port 3000` where

-   `--hot` Enable webpack's Hot Module Replacement feature
-   `--port 3000` - use port 3000 instead of default 8000
-   inline (default) a script will be inserted in your bundle to take care of reloading, and build messages will appear in the browser console.

One alternative is to run `webpack-dev-server --hot --colors --host=0.0.0.0 --port 3000` which will enable your dev server to be reached from other computers on your local network
