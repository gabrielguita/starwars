const apiDetails = {
    url: "https://swapi.co/api/people/"
};

let starwarsDataObj = [];
const containerSelect = document.querySelector(".container tbody");

const catalogElm = data => {
    let trElm = document.createElement("tr");
    let tdElm = document.createElement("td");
    let tdname = document.createElement("td");
    let secondTdElm = document.createElement("td");
    let aElm = document.createElement("a");

    tdElm.innerHTML += data.name;
    secondTdElm.innerHTML += data.gender;
    aElm.innerHTML += data.name;
    aElm.href += `${window.location.href +
        "people.html?name=" +
        data.name.replace(/\s+/g, "-").toLowerCase()}`;
    tdElm.innerHTML += data.gender;

    tdname.appendChild(aElm);
    trElm.appendChild(tdname);
    trElm.appendChild(secondTdElm);

    if (containerSelect) containerSelect.appendChild(trElm);
};

const peopleDetails = data => {
    if (document.querySelector(".details")) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const param = urlParams.get("name");
        const details =
            data &&
            data.filter(
                x => x.name.replace(/\s+/g, "-").toLowerCase() === param
            );
        if (details.length > 0) {
            document.querySelector(".header h1").innerHTML = details[0].name;
            document.querySelector(".eye-color span").innerHTML =
                details[0].eye_color;
            document.querySelector(".height span").innerHTML =
                details[0].height;
            document.querySelector(".gender span").innerHTML =
                details[0].gender;
            document.querySelector(".spinner").classList.remove("hide");
            fetchPlanet(details[0].homeworld);
        }
    }
};

//sort function
function compareValues(key, order = "asc") {
    return function innerSort(a, b) {
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            return 0;
        }

        const varA = typeof a[key] === "string" ? a[key].toUpperCase() : a[key];
        const varB = typeof b[key] === "string" ? b[key].toUpperCase() : b[key];

        let comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }

        return order === "desc" ? comparison * -1 : comparison;
    };
}

if (document.querySelector("#sortByName")) {
    document
        .querySelector("#sortByName")
        .addEventListener("click", function(e) {
            e.preventDefault();
            containerSelect.querySelectorAll("tr").forEach(n => n.remove());
            starwarsDataObj.sort(compareValues("name")).map(x => catalogElm(x));
        });
}

if (document.querySelector("#sortByGender")) {
    document
        .querySelector("#sortByGender")
        .addEventListener("click", function(e) {
            e.preventDefault();
            containerSelect.querySelectorAll("tr").forEach(n => n.remove());
            starwarsDataObj
                .sort(compareValues("gender"))
                .map(x => catalogElm(x));
        });
}

/** fetch data  **/
const fetchData = url =>
    fetch(url)
        .then(response => response.json())
        .then(req => funcSuccess(req))
        .catch(err => console.log("error is", err.message));

/** fetch data  **/
const fetchPlanet = url =>
    fetch(url)
        .then(response => response.json())
        .then(req => funcPlannetSuccess(req))
        .catch(err => console.log("error is", err.message));

/** fetch data  **/
const fetchResidents = url =>
    fetch(url)
        .then(response => response.json())
        .then(req => funcPlannetResidentsSuccess(req))
        .catch(err => console.log("error is", err.message));

const funcPlannetSuccess = data => {
    const plannet = document.querySelector(".plannet");
    let name = document.createElement("span");
    name.innerHTML += data.name;
    plannet.appendChild(name);

    data.residents.forEach(function(x) {
        fetchResidents(x);
    });
    document.querySelector(".spinner").classList.add("hide");
};

const funcPlannetResidentsSuccess = data => {
    const ul = document.querySelector(".residents ul");
    let liname = document.createElement("li");
    let aElm = document.createElement("a");
    aElm.innerHTML += data.name;
    aElm.href += `${window.location.origin +
        "/people.html?name=" +
        data.name.replace(/\s+/g, "-").toLowerCase()}`;

    ul.appendChild(liname).appendChild(aElm);
    peopleDetails(data);
    document.querySelector(".spinner").classList.add("hide");
};

// fetch success
const funcSuccess = data => {
    if (!data) return null;
    starwarsDataObj = data.results;
    data.results.map(photoObj => catalogElm(photoObj));
    peopleDetails(starwarsDataObj);
    document.querySelector(".spinner").classList.add("hide");
};

//initial load
window.addEventListener("load", function() {
    fetchData(apiDetails.url);
    TableFilter.init();
});

const TableFilter = (function() {
    let Arr = Array.prototype;
    let input;

    function onInputEvent(e) {
        input = e.target;
        const table1 = document.getElementsByClassName(
            input.getAttribute("data-table")
        );

        Arr.forEach.call(table1, function(table) {
            Arr.forEach.call(table.tBodies, function(tbody) {
                Arr.forEach.call(tbody.rows, filter);
            });
        });
    }

    function filter(row) {
        const text = row.textContent.toLowerCase();
        const val = input.value.toLowerCase();
        row.style.display = text.indexOf(val) === -1 ? "none" : "table-row";
    }

    return {
        init: function() {
            let inputs = document.getElementsByClassName("table-filter");
            Arr.forEach.call(inputs, function(input) {
                input.oninput = onInputEvent;
            });
        }
    };
})();
