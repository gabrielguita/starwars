const path = require("path");
const webpack = require("webpack");
const merge = require("webpack-merge");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

var common = {
    // entry: "./src/index.js",
    output: {
        path: path.join(__dirname, "dist"),
        publicPath: "/"
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: "src/index.html",
            inject: "body"
        }),
        new HTMLWebpackPlugin({
            filename: "people.html",
            inject: true,
            // chunks: ['app', 'vendor']
            template: "src/people.html"
        })
    ],

    resolve: {
        modules: [path.join(__dirname, "src"), "node_modules"],
        extensions: [".js", ".scss", ".css", ".png"]
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                exclude: [/node_modules/],
                loaders: [MiniCssExtractPlugin.loader, "css-loader?url=false"]
            },
            {
                test: /\.scss$/,
                exclude: [/node_modules/],
                // see https://github.com/webpack-contrib/css-loader#url
                loaders: ["style-loader", "css-loader?url=false", "sass-loader"]
            },
            {
                test: /\.css$/,
                exclude: [/node_modules/],
                loaders: ["style-loader", "css-loader?url=false"]
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                exclude: [/node_modules/],
                loader: "url-loader",
                options: {
                    limit: 10000,
                    mimetype: "application/font-woff"
                }
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                exclude: [/node_modules/],
                loader: "file-loader"
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                exclude: [/node_modules/],
                loader: "file-loader"
            }
        ]
    }
};

module.exports = merge(common, {
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                exclude: [/node_modules/],
                loaders: [MiniCssExtractPlugin.loader, "css-loader?url=false"]
            },
            {
                test: /\.scss$/,
                exclude: [/node_modules/],
                loaders: [
                    MiniCssExtractPlugin.loader,
                    "css-loader?url=false",
                    "sass-loader"
                ]
            }
        ]
    },
    devServer: {
        inline: true,
        stats: "errors-only",
        contentBase: path.join(__dirname, "src/assets"),
        historyApiFallback: true
    }
});
